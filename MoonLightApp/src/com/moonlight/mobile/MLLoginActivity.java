package com.moonlight.mobile;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.moonlight.mobile.data.LoginCacheInfo;
import com.moonlight.mobile.login.LoginValidate;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class MLLoginActivity extends Activity {
	private final static String LOG_TAG = "com.moonlight.mobile.MLLoginActivity";
	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	private static final String[] DUMMY_CREDENTIALS = new String[] {
			"tivx016.cn.ibm.com", "administrator","Zhu88jie" };

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for email and password at the time of the login attempt.
	private String mHostName = "9.123.141.243";
	private String mUserName = "administrator";
	private String mPassword = "Zhu88jie";
	private String mCellNumber = "00000000000";

	// UI references.
	private EditText    mCellView;
	private EditText 	mHostView;
	private EditText 	mUserView;
	private EditText 	mPasswordView;
	private View 		mLoginFormView;
	private View 		mLoginStatusView;
	private TextView 	mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);
		setupActionBar();

		// Set up the login form.
		//mHostName = getIntent().getStringExtra(EXTRA_EMAIL);
		
		TelephonyManager tMgr = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);		
		String str = "";
		str += "DeviceId(IMEI) = " + tMgr.getDeviceId() + "\n";
		str += "DeviceSoftwareVersion = " + tMgr.getDeviceSoftwareVersion()
				+ "\n";
		str += "Line1Number = " + tMgr.getLine1Number() + "\n";
		str += "NetworkCountryIso = " + tMgr.getNetworkCountryIso() + "\n";
		str += "NetworkOperator = " + tMgr.getNetworkOperator() + "\n";
		str += "NetworkOperatorName = " + tMgr.getNetworkOperatorName() + "\n";
		str += "NetworkType = " + tMgr.getNetworkType() + "\n";
		str += "honeType = " + tMgr.getPhoneType() + "\n";
		str += "SimCountryIso = " + tMgr.getSimCountryIso() + "\n";
		str += "SimOperator = " + tMgr.getSimOperator() + "\n";
		str += "SimOperatorName = " + tMgr.getSimOperatorName() + "\n";
		str += "SimSerialNumber = " + tMgr.getSimSerialNumber() + "\n";
		str += "SimState = " + tMgr.getSimState() + "\n";
		str += "SubscriberId(IMSI) = " + tMgr.getSubscriberId() + "\n";
		str += "VoiceMailNumber = " + tMgr.getVoiceMailNumber() + "\n";
		    
		Log.d(LOG_TAG, str);
		
		
		mCellView = (EditText) findViewById(R.id.phonenumber);
		mCellView.setText(mCellNumber);
		
		mHostView = (EditText) findViewById(R.id.hostname);
		mHostView.setText(mHostName);
		
		mUserView = (EditText) findViewById(R.id.username);
		mUserView.setText(mUserName);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);
		

		

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			// TODO: If Settings has multiple levels, Up should navigate up
			// that hierarchy.
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.mllogin, menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mCellView.setError(null);
		mHostView.setError(null);
		mUserView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mCellNumber = mCellView.getText().toString();
		mHostName = mHostView.getText().toString();
		mUserName = mUserView.getText().toString();
		mPassword = mPasswordView.getText().toString();
		
		LoginCacheInfo lciInstance = LoginCacheInfo.getInstance(mHostName, mUserName, mPassword, "/Moonlight", "8080");

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// TODO validate the host name
		if (TextUtils.isEmpty(mHostName)) {
			mHostView.setError(getString(R.string.error_field_required));
			focusView = mHostView;
			cancel = true;
		}
		// TODO validate the user name
		if (TextUtils.isEmpty(mUserName)) {
			mUserView.setError(getString(R.string.error_field_required));
			focusView = mUserView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {
				// Simulate network access.
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				return false;
			}

			//TODO add code to check if user name and password are valid for loginning.
			
			//return DUMMY_CREDENTIALS[0].equals(mHostName) &&DUMMY_CREDENTIALS[1].equals(mUserName) && DUMMY_CREDENTIALS[2].equals(mPassword);
			//
			return LoginValidate.getInstance().checkUser(mHostName,mUserName,mPassword);
			//TODO: register the new account here.
			
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) {
				finish();
				Intent mainIntent = new Intent(getApplicationContext(),com.moonlight.mobile.MoonLightMainActivity.class);
				startActivity(mainIntent);
			} else {
				mPasswordView
						.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
}
