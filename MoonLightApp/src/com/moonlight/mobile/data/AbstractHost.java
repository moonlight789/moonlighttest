package com.moonlight.mobile.data;

public class AbstractHost {
	private String name;
	private String configName;
	private long overallCpuUsage;
	private long overallMemoryUsage;
	private String cpuModel;
	private String osType;
	private long memorySize;
	private String hardwareModule;
	private String vmotionEnabled;
	private long cpuMHz;
	private String uuid;
	private int numCPuCores;
	private int numNics;
	private String vendor;
	private String productLineId;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getOverallCpuUsage() {
		return overallCpuUsage;
	}
	public void setOverallCpuUsage(long overallCpuUsage) {
		this.overallCpuUsage = overallCpuUsage;
	}
	public long getOverallMemoryUsage() {
		return overallMemoryUsage;
	}
	public void setOverallMemoryUsage(long overallMemoryUsage) {
		this.overallMemoryUsage = overallMemoryUsage;
	}
	public String getCpuModel() {
		return cpuModel;
	}
	public void setCpuModel(String cpuModel) {
		this.cpuModel = cpuModel;
	}
	public String getOsType() {
		return osType;
	}
	public void setOsType(String osType) {
		this.osType = osType;
	}
	public long getMemorySize() {
		return memorySize;
	}
	public void setMemorySize(long memorySize) {
		this.memorySize = memorySize;
	}
	public String getHardwareModule() {
		return hardwareModule;
	}
	public void setHardwareModule(String hardwareModule) {
		this.hardwareModule = hardwareModule;
	}
	public String getVmotionEnabled() {
		return vmotionEnabled;
	}
	public void setVmotionEnabled(String vmotionEnabled) {
		this.vmotionEnabled = vmotionEnabled;
	}
	public long getCpuMHz() {
		return cpuMHz;
	}
	public void setCpuMHz(long cpuMHz) {
		this.cpuMHz = cpuMHz;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public int getNumCPuCores() {
		return numCPuCores;
	}
	public void setNumCPuCores(int numCPuCores) {
		this.numCPuCores = numCPuCores;
	}
	public int getNumNics() {
		return numNics;
	}
	public void setNumNics(int numNics) {
		this.numNics = numNics;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}	
	
}


