package com.moonlight.mobile.data;

public class LoginCacheInfo {
	private  static LoginCacheInfo instance = null;
	private  String hostName;
	private  String userName;
	private  String password;
	private  String portNumber;
	private  String baseURL;
	private  String hostBaseURL;
	private  String vmBaseURL;

	
	public  String getHostName() {
		return hostName;
	}
	public  void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public  String getUserName() {
		return userName;
	}
	public  void setUserName(String userName) {
		this.userName = userName;
	}
	public  String getPassword() {
		return password;
	}
	public  void setPassword(String password) {
		this.password = password;
	}
	public  String getPortNumber() {
		return portNumber;
	}
	public  void setPortNumber(String portNumber) {
		this.portNumber = portNumber;
	}
	public  String getBaseURL() {
		return baseURL;
	}
	
	public String getHostBaseURL() {
		return hostBaseURL;
	}
	public void setHostBaseURL(String hostBaseURL) {
		this.hostBaseURL = hostBaseURL;
	}
	public String getVmBaseURL() {
		return vmBaseURL;
	}
	public void setVmBaseURL(String vmBaseURL) {
		this.vmBaseURL = vmBaseURL;
	}
	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}
	
	
	public static synchronized LoginCacheInfo getInstance(String hostName, String userName, String password, String baseURL, String portNumber){
		if(instance == null){
			instance = new LoginCacheInfo();
		}
		instance.setHostName(hostName);
		instance.setPassword(password);
		instance.setPortNumber(portNumber);
		instance.setUserName(userName);
		instance.setBaseURL(baseURL);
		StringBuilder strURL = new StringBuilder();
        strURL.append("http://").append(hostName).append(":").append(portNumber).append(baseURL).append("/");
        instance.setBaseURL(strURL.toString());        
        instance.setHostBaseURL(strURL.toString() + "hostsystems");
        instance.setVmBaseURL(strURL.toString() + "virtualmachines");   
        
		return instance;		
	}
	
	public String getVMRestURL(){
		StringBuilder sb = new StringBuilder(instance.getVmBaseURL());
		sb.append("?user=").append(instance.getUserName());
		return sb.toString();
	}
	
	public static String getHostRestURL(){
		StringBuilder sb = new StringBuilder(instance.getHostBaseURL());
		sb.append("?user=").append(instance.getUserName());
		return sb.toString();
	}
	public String getVMInfoRestURLByName(String vmName){
		StringBuilder sb = new StringBuilder(instance.getVmBaseURL());
		if(!(vmName == null || vmName.equals(""))){			
			sb.append("/query?user=").append(userName).append("&where=(name=").append(vmName).append(")");			
		}
		return sb.toString();
	}
	public String getHostInfoRestURLByName(String hostName){
		StringBuilder sb = new StringBuilder(instance.getVmBaseURL());
		if(!(hostName == null || hostName.equals(""))){
			sb.append("/query?user=").append(userName).append("&where=(runtime.host=").append(hostName).append(")");	
		}
		return sb.toString();
	}
	public static synchronized LoginCacheInfo getInstance(){
		if(instance == null){
			instance = new LoginCacheInfo();
			instance.setHostName("localhost");
			instance.setPassword("");
			instance.setPortNumber("8080");
			instance.setUserName("");
			instance.setBaseURL("/Moonlight");
		}
		
		return instance;
		
	}
	public String getAlarmInfoRestURL(String name, int type) {
		StringBuilder sb = new StringBuilder(instance.getBaseURL());
		if(!(name == null || name.equals(""))){
			sb.append("/alarms/query?user=").append(userName).append("&name=").append(name);				
		}
		return sb.toString();
	}
	
	
}
