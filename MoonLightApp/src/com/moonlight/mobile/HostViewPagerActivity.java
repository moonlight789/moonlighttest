package com.moonlight.mobile;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.moonlight.mobile.fragment.HostAlarmFragment;
import com.moonlight.mobile.fragment.HostPerfFragment;
import com.moonlight.mobile.fragment.HostSummaryFragment;
import com.moonlight.mobile.fragment.HostVMListFragment;
import com.moonlight.mobile.global.Constants;

public class HostViewPagerActivity extends Activity implements OnRefreshListener<ListView>, ActionBar.TabListener, OnItemClickListener {	
	private static final String LOG_TAG = "HostViewPagerActivity";
	private ViewPager mViewPager;
	private static String hostName;
	private static JSONObject hostInfo;
	
 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_host_list_in_vp);
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
        .detectDiskReads()
        .detectDiskWrites()
        .detectNetwork()
        .permitNetwork()
        .penaltyLog()
        .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
        .detectLeakedSqlLiteObjects()
        .detectLeakedClosableObjects()
        .detectActivityLeaks()
        .penaltyLog()
        .penaltyDeath()
        .build());
		
		mViewPager = (ViewPager) findViewById(R.id.vp_list);
		mViewPager.setAdapter(new ListViewPagerAdapter(getFragmentManager()));
		
		
		
		
		//Init actionbar
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayHomeAsUpEnabled(true);
		for (int i = 0; i < mViewPager.getAdapter().getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mViewPager.getAdapter().getPageTitle(i))
					.setTabListener(this));
		}		
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
			public void onPageSelected(int pos){
				getActionBar().setSelectedNavigationItem(pos);
				
			}
		});
	}

	@Override
	protected void onStart() {
		SharedPreferences spf = getSharedPreferences(Constants.MOONLIGHT_PREFS_NAME,0);
		this.hostName = spf.getString("esxi_hostname", "");

		String strHostInfo = spf.getString("esxi_host_summary","");		
		try {
			hostInfo = new JSONObject(strHostInfo);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		super.onStart();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

        return super.onCreateOptionsMenu(menu);
	}

	private class ListViewPagerAdapter extends FragmentPagerAdapter {

		public ListViewPagerAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);

		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return getString(R.string.title_host_summary);
			case 1:
				return getString(R.string.title_host_vm_list);
			case 2:
				return getString(R.string.title_host_alarms);
			case 3:
				return getString(R.string.title_host_performance);
			default:
				return getString(R.string.title_host_reserved);
			}  
			
		}
		

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			// TODO Auto-generated method stub
			return super.instantiateItem(container, position);
		}

		@Override
		public int getCount() {
			return 4;
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
	        case 0:
	            return new HostSummaryFragment(hostInfo);
	        case 1:
	            return new HostVMListFragment ();
	        case 2:
	            return new HostAlarmFragment();
	        case 3:
	        	return new HostPerfFragment();
			}
			return null;
		}
		
		

	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		mViewPager.getAdapter().instantiateItem(mViewPager, mViewPager.getCurrentItem());
		mViewPager.getAdapter().notifyDataSetChanged();
		new GetDataTask(refreshView).execute();
	}

	private static class GetDataTask extends AsyncTask<Void, Void, Void> {

		PullToRefreshBase<?> mRefreshedView;
//		int tabPos;
//		PullToRefreshBase<ListView> mListView;

		public GetDataTask(PullToRefreshBase<?> refreshedView) {
			this.mRefreshedView = refreshedView;
//			this.tabPos = tabPos;
//			this.mListView = (PullToRefreshBase<ListView>) listView;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// Simulates a background job.
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			mRefreshedView.onRefreshComplete();
			super.onPostExecute(result);
		}
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {

		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}	


	@Override
	public void onItemClick(AdapterView<?> arg0, View vmListView, int arg2, long arg3) {
		Log.i(LOG_TAG, "Current page item is: " + mViewPager.getCurrentItem());
		int position = mViewPager.getCurrentItem();
		
		if(position == 1)
		{	String vmName = (String) ((TextView)vmListView).getText();
			Intent vmIntent = new Intent(this,com.moonlight.mobile.VMViewPagerActivity.class);
			SharedPreferences spf = this.getSharedPreferences(Constants.MOONLIGHT_PREFS_NAME, 0);
			Editor spe = spf.edit();
			spe.putString("vm_name", vmName);
			spe.commit();
			this.startActivity(vmIntent);
		}		
	}

}
