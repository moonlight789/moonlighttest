package com.moonlight.mobile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.moonlight.rest.client.HTTPDataDelegate;

public class MLEventsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mlevents);		
		
		ListView eventListView = (ListView)findViewById(R.id.event_list);	
		String alarmId = getIntent().getStringExtra("alarm_id");	
		String vmName = getIntent().getStringExtra("vm_name");
		//JSONArray eventArray = HTTPDataDelegate.fetchEvents(vmName,alarmId);
		
		
		
		List<JSONObject> list = new ArrayList<JSONObject>();  
//		for(int i=0;eventArray!= null && i< eventArray.length()  ;i++){  
//            try {
//				list.add(eventArray.getJSONObject(i));
//			} catch (JSONException e) {					
//			} 
//        } 
        
		try {
			JSONObject job = new JSONObject("{\"host\":\"tivvm165.cn.ibm.com\",\"vm\":\"APMSmartPoC-Server-VM1\",\"message\":\"Alarm 'Virtual machine cpu usage' on APMSmartPoC-Server-VM1 changed from Green to Yellow\",\"severity\":\"info\",\"username\":\"\",\"alarm\":\"alarm-6\",\"occured.count\":5,\"occured.lasttime\":\"2014-05-07T19:47:23.304999Z\",\"occured.firsttime\":\"2014-05-07T19:47:23.304999Z\",\"occured.timelist\":\"2014-05-07T19:47:23.304999Z#2014-05-07T19:47:23.304999Z#2014-05-07T19:47:23.304999Z#2014-05-07T19:47:23.304999Z#2014-05-07T19:47:23.304999Z\"}");
			list.add(job);
		} catch (JSONException e) {
			Toast.makeText(this.getApplicationContext(),"No Event data", 3).show();
		}			
        
		
		List<HashMap<String, Object>> data = new ArrayList<HashMap<String,Object>>();
		for(JSONObject obj : list){
        	HashMap<String, Object> item = new HashMap<String, Object>();
        	try {
				item.put("eventvm", obj.get("vm").toString());
				item.put("eventmessage", obj.get("message").toString());
	        	item.put("eventcount", obj.get("occured.count").toString());
	        	item.put("eventlasttime", obj.get("occured.lasttime").toString());
	        	data.add(item);
			} catch (JSONException e) {
				e.printStackTrace();
			}       	
        	
        }
        
       //创建SimpleAdapter适配器将数据绑定到item显示控件上  
       SimpleAdapter adapter = new SimpleAdapter(this, data, R.layout.fragment_mlevents, new String[]{"eventvm", "eventmessage","eventlasttime","eventcount"}, new int[]{R.id.event_vm, R.id.event_message, R.id.event_lasttime,R.id.event_occurcount});  
       //实现列表的显示  
       eventListView.setAdapter(adapter);  
       //条目点击事件  
       
        
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.mlevents, menu);
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id){
		case R.id.action_settings:
			return true;
		case android.R.id.home:			
			break;
		default:
			break;
		
		}
		
		return super.onOptionsItemSelected(item);
	}


}
