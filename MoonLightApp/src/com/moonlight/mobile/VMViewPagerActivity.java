package com.moonlight.mobile;

import java.util.Locale;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.moonlight.mobile.fragment.VMAlarmFragment;
import com.moonlight.mobile.fragment.VMPerfFragment;
import com.moonlight.mobile.fragment.VMSummaryFragment;
import com.moonlight.mobile.global.Constants;

public class VMViewPagerActivity extends FragmentActivity implements
		ActionBar.TabListener {
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v13.app.FragmentStatePagerAdapter}.
	 */
	VMPagerAdapter mVMPagerAdapter;
	
	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager 	 mViewPager;
	
	private String vmName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vmview_pager);

		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		SharedPreferences spf = getSharedPreferences(Constants.MOONLIGHT_PREFS_NAME, 0);
		vmName = spf.getString("vm_name", "");
		if(vmName == null || vmName.equals("")){
			vmName = this.getIntent().getStringExtra("vm_name");
		}
        
		mVMPagerAdapter = new VMPagerAdapter(getFragmentManager(),vmName);

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.vm_view_pager);
		mViewPager.setAdapter(mVMPagerAdapter);
		mViewPager.setCurrentItem(0);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {						
						actionBar.setSelectedNavigationItem(position);
					}
				});
		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mVMPagerAdapter.getCount(); i++) {			
			actionBar.addTab(actionBar.newTab()
					.setText(mVMPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id){
		case R.id.action_settings:
			return true;
		case android.R.id.home:			
			break;
		default:
			break;
		
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {		
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class VMPagerAdapter extends FragmentPagerAdapter {
		
		private VMSummaryFragment   vmSummaryFragment = null;
		private VMAlarmFragment vmAlarmFragment = null ;
		private VMPerfFragment vmPerfFragment = null;
		
//
//		public VMPagerAdapter(FragmentManager fm) {
//			super(fm);
//			
//		}
		public VMPagerAdapter(FragmentManager fm,String vmName){
			super(fm);	
			vmSummaryFragment = new VMSummaryFragment(vmName);
			vmAlarmFragment = new VMAlarmFragment();
			vmPerfFragment =  new VMPerfFragment();

		}		

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;			
			switch(position){
			case 0:
				fragment =  vmSummaryFragment;
				break;
			case 1:
				fragment = vmAlarmFragment;
				break;
			case 2: 
				fragment = vmPerfFragment;
				break;
			}
			return fragment;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_vm_summary);
			case 1:
				return getString(R.string.title_vm_alarms);
			case 2:
				return getString(R.string.title_vm_performance);
			}
			return null;
		}
	}

	
		
}



		
