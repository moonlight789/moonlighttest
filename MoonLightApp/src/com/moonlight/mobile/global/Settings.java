package com.moonlight.mobile.global;

public class Settings {
	private final static Settings instance = new Settings();

	private String port = "8080";
	private String baseURLRoot = "/Moonlight/";
	private int refreshFreq = 30;
	
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getBaseURLRoot() {
		return baseURLRoot;
	}
	public void setBaseURL(String baseURLRoot) {
		this.baseURLRoot = baseURLRoot;
	}
	public int getRefreshFreq() {
		return refreshFreq;
	}
	public void setRefreshFreq(int refreshFreq) {
		this.refreshFreq = refreshFreq;
	}
	
	public static synchronized Settings getInstance(){
		return instance;		
	}
	
}
