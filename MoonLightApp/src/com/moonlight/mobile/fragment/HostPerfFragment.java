package com.moonlight.mobile.fragment;

import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.moonlight.mobile.R;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;


public class HostPerfFragment extends AbstractVMWareFragment {
	private String LOG_TAG = "com.moonlight.mobile.fragment.HostPerfFragment";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		Log.d(LOG_TAG,"initialize the view");
		
		Context context = container.getContext();		
		ArrayAdapter adapter = new ArrayAdapter<String>(container.getContext(), android.R.layout.simple_list_item_1,new String[]{"Performance 1","Performance 2"});
		PullToRefreshListView		plv = (PullToRefreshListView) LayoutInflater.from(context).inflate(R.layout.fragment_host_viewpager, container, false);
		plv.setOnRefreshListener((OnRefreshListener<ListView>) this.getActivity());
		plv.setOnItemClickListener((OnItemClickListener) this.getActivity());
		plv.setAdapter(adapter);		
		return plv;
	}

}
