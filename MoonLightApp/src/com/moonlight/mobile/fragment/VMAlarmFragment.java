package com.moonlight.mobile.fragment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.moonlight.mobile.R;
import com.moonlight.mobile.adapter.AlarmAdapter;
import com.moonlight.rest.client.HTTPDataDelegate;


public class VMAlarmFragment extends AbstractVMWareFragment {
	private String LOG_TAG = "com.moonlight.mobile.fragment.VMAlarmFragment";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		Log.d(LOG_TAG,"initialize the view");
		Context context = container.getContext();
		View rootView = inflater.inflate(R.layout.vm_alarm, container,false);
		
		
		ListView alarmListView = (ListView) rootView.findViewById(R.id.alarm_list);	
		String vmName = this.getActivity().getIntent().getStringExtra("vm_name");			
		JSONArray alarmArray = HTTPDataDelegate.fetchAlarms(vmName,0);
		
		List<JSONObject> list = new ArrayList<JSONObject>();  
		for(int i=0;alarmArray!= null && i< alarmArray.length()  ;i++){  
            try {
				list.add(alarmArray.getJSONObject(i));
			} catch (JSONException e) {					
			} 
        } 
        
		try {
			JSONObject job = new JSONObject("{\"alarm.key\":\"alarm-6\",\"alarm.name\":\"Virtual machine cpu usage\",\"alarm.status\":\"yellow\",\"alarm.acknowledgedtime\":\"\",\"alarm.triggeredtime\":\"2014-05-07T19:47:22.17938Z\",\"alarm.acknowledged\":false,\"alarm.acknowledgedbyuser\":\"\"}");
			list.add(job);
		} catch (JSONException e) {
			Toast.makeText(rootView.getContext(),"No Alarm data", 3).show();
		}
		
        AlarmAdapter adapter = new AlarmAdapter(context,list);  
        alarmListView.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
        
        return rootView;
       
	}

}
