package com.moonlight.mobile.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moonlight.mobile.R;


public class VMPerfFragment extends AbstractVMWareFragment {
	private String LOG_TAG = "com.moonlight.mobile.fragment.VMPerfFragment";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		Log.d(LOG_TAG,"initialize the view");
		View rootView = inflater.inflate(R.layout.fragment_vmview_pager, container,false);
		
		TextView textView = (TextView) rootView.findViewById(R.id.section_label);
		String vmName = this.getActivity().getIntent().getStringExtra("vm_name");
		textView.setText(vmName + " Performance");
        
        return rootView;
	}

}
