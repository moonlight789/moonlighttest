package com.moonlight.mobile.fragment;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moonlight.mobile.R;
import com.moonlight.rest.client.HTTPDataDelegate;

@SuppressLint("ValidFragment")
public class VMSummaryFragment extends AbstractVMWareFragment {
	private final static String LOG_TAG = "com.moonlight.mobile.fragment.HostSummaryFragment";
	private JSONObject vmSummaryObj;
	private String vmName;
	
	private String vmSummaryString= "{\"config.guestFullName\":\"Other (32-bit)\",\"summary.quickStats.guestMemoryUsage\":\"573\",\"summary.quickStats.overallCpuUsage\":\"767\",\"summary.quickStats.hostMemoryUsage\":\"8116\",\"runtime.powerState\":\"POWERED_ON\",\"runtime.memoryOverhead\":\"82456576\",\"summary.quickStats.overallCpuDemand\":\"815\",\"runtime.bootTime\":\"2014-04-09T07:17:51.601481Z\",\"runtime.host\":\"9.123.103.72\",\"name\":\"tivvm189_SaaS_AllInOne\",\"runtime.maxMemoryUsage\":\"8192\",\"runtime.maxCpuUsage\":\"4798\",\"runtime.connectionState\":\"CONNECTED\"}";
	
	
	public VMSummaryFragment(String vmName) {
		try {
			this.vmSummaryObj = new JSONObject(vmSummaryString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.vmName = vmName;

	}

//	@Override
//	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//		
//		super.onCreateOptionsMenu(menu, inflater);
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.vm_summary,container, false);
		vmSummaryObj = HTTPDataDelegate.fetchSingleVMData(vmName);
		if(vmSummaryObj == null) return rootView;
		
		TextView textView;		
		try {
			textView = (TextView) rootView.findViewById(R.id.config_guestfullname_text);
			textView.setText(vmSummaryObj.getString("config.guestFullName"));
			
			
			textView = (TextView) rootView.findViewById(R.id.summary_quickstats_guestmemoryusage_text);
			textView.setText(vmSummaryObj.getString("summary.quickStats.guestMemoryUsage"));
			
			textView = (TextView) rootView.findViewById(R.id.summary_quickstats_overallcpuusage_text);
			textView.setText(vmSummaryObj.getString("summary.quickStats.overallCpuUsage"));
			
			textView = (TextView) rootView.findViewById(R.id.summary_quickstats_hostmemoryusage_text);
			textView.setText(vmSummaryObj.getString("summary.quickStats.hostMemoryUsage"));
			
			textView = (TextView) rootView.findViewById(R.id.runtime_powerstate_text);
			textView.setText(vmSummaryObj.getString("runtime.powerState"));
			
			textView = (TextView) rootView.findViewById(R.id.runtime_memoryoverhead_text);
			textView.setText(vmSummaryObj.getString("runtime.memoryOverhead"));
			
			textView = (TextView) rootView.findViewById(R.id.summary_quickstats_overallcpudemand_text);
			textView.setText(vmSummaryObj.getString("summary.quickStats.overallCpuDemand"));
			
			//textView = (TextView) rootView.findViewById(R.id.runtime_boottime_text);
			//textView.setText(vmSummaryObj.getString("runtime.bootTime"));
			
			textView = (TextView) rootView.findViewById(R.id.runtime_host_text);
			textView.setText(vmSummaryObj.getString("runtime.host"));
			
			textView = (TextView) rootView.findViewById(R.id.name_text);
			textView.setText(vmSummaryObj.getString("name"));
			
			textView = (TextView) rootView.findViewById(R.id.runtime_maxmemoryusage_text);
			textView.setText(vmSummaryObj.getString("runtime.maxMemoryUsage"));
			
			textView = (TextView) rootView.findViewById(R.id.runtime_maxcpuusage_text);
			textView.setText(vmSummaryObj.getString("runtime.maxCpuUsage"));
			
			textView = (TextView) rootView.findViewById(R.id.runtime_connectionstate_text);
			textView.setText(vmSummaryObj.getString("runtime.connectionState"));

		} catch (JSONException e) {			
			Log.e(LOG_TAG, e.getMessage(), e);
		}
		return rootView;
	}
	

}
