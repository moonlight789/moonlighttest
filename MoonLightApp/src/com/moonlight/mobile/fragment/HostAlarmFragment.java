package com.moonlight.mobile.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.moonlight.mobile.R;


public class HostAlarmFragment extends AbstractVMWareFragment {
	private String LOG_TAG = "com.moonlight.mobile.fragment.HostAlarmFragment";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		Log.d(LOG_TAG,"initialize the view");
		Context context = container.getContext();
		ArrayAdapter adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,new String[]{"Alarm1","Alarm2"});
		PullToRefreshListView		plv = (PullToRefreshListView) LayoutInflater.from(context).inflate(R.layout.fragment_host_viewpager, container, false);
		plv.setOnRefreshListener((OnRefreshListener<ListView>) this.getActivity());
		plv.setOnItemClickListener((OnItemClickListener) this.getActivity());
		plv.setAdapter(adapter);		
		return plv;
	}

}
