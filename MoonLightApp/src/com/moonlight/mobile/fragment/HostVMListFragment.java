package com.moonlight.mobile.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.moonlight.mobile.R;
import com.moonlight.mobile.global.Constants;
import com.moonlight.rest.client.HTTPDataDelegate;


public class HostVMListFragment extends AbstractVMWareFragment {
	
	private final static String LOG_TAG = "com.moonlight.mobile.fragment.HostVMListFragment";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Context context = container.getContext();
		
		SharedPreferences spf = this.getActivity().getSharedPreferences(Constants.MOONLIGHT_PREFS_NAME,0);
		String hostName = spf.getString("esxi_hostname", "");

		
		
		
		JSONArray vmJsonObjArray = HTTPDataDelegate.fetchVMData(hostName);
		String[] vmNameArray = null;
		
		ArrayList<String> vmNameList = new ArrayList<String>();
		for (int i = 0; vmJsonObjArray != null && i < vmJsonObjArray.length(); i++) {
			try {
				vmNameList.add((String) vmJsonObjArray.getJSONObject(i).getString("name"));
				vmNameArray = new String[vmNameList.size()];
				vmNameList.toArray(vmNameArray);
			} catch (JSONException e) {				
				Log.e(LOG_TAG, e.getMessage(), e);		
			}
		} 
				
		ArrayAdapter adapter = new ArrayAdapter<String>(container.getContext(), android.R.layout.simple_list_item_1,vmNameArray);
		PullToRefreshListView plv = (PullToRefreshListView) LayoutInflater.from(context).inflate(R.layout.fragment_host_viewpager, container, false);
		plv.setOnRefreshListener((OnRefreshListener<ListView>) this.getActivity());
		plv.setOnItemClickListener((OnItemClickListener) this.getActivity());
		plv.setAdapter(adapter);		
		return plv;
		
	}

}
