package com.moonlight.mobile.fragment;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moonlight.mobile.R;

@SuppressLint("ValidFragment")
public class HostSummaryFragment extends AbstractVMWareFragment {
	private String LOG_TAG = "com.moonlight.mobile.fragment.HostSummaryFragment";
	
	
	JSONObject hostSummary;
	public HostSummaryFragment(JSONObject hostSummaryObj) {
		this.hostSummary = hostSummaryObj;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 
		View rootView = inflater.inflate(R.layout.host_full_summary,container, false);
		TextView textView;		
		try {
			textView = (TextView) rootView.findViewById(R.id.summary_quickstats_overallmemoryusage_text);
			textView.setText(hostSummary.getString("summary.quickStats.overallMemoryUsage"));
			textView = (TextView) rootView.findViewById(R.id.summary_vm_count_text);
			textView.setText(hostSummary.getString("vm.count"));
			
			textView = (TextView) rootView.findViewById(R.id.summary_hardware_cpumodel_text);
			textView.setText(hostSummary.getString("summary.hardware.cpuModel"));
			
			textView = (TextView) rootView.findViewById(R.id.summary_config_product_ostype_text);
			textView.setText(hostSummary.getString("summary.config.product.osType"));
			
			textView = (TextView) rootView.findViewById(R.id.summary_quickstats_overallcpuusage_text);
			textView.setText(hostSummary.getString("summary.quickStats.overallCpuUsage"));
			
			textView = (TextView) rootView.findViewById(R.id.runtime_powerstate_text);
			textView.setText(hostSummary.getString("runtime.powerState"));
			
			textView = (TextView) rootView.findViewById(R.id.summary_hardware_memorysize_text);
			textView.setText(hostSummary.getString("summary.hardware.memorySize"));
			
			textView = (TextView) rootView.findViewById(R.id.summary_config_name_text);
			textView.setText(hostSummary.getString("summary.config.name"));
			
			textView = (TextView) rootView.findViewById(R.id.summary_hardware_cpumhz_text);
			textView.setText(hostSummary.getString("summary.hardware.cpuMhz"));
			
			textView = (TextView) rootView.findViewById(R.id.summary_hardware_numcpucores_text);
			textView.setText(hostSummary.getString("summary.hardware.numCpuCores"));
			
			textView = (TextView) rootView.findViewById(R.id.runtime_boottime_text);
			textView.setText(hostSummary.getString("runtime.bootTime"));
			
			textView = (TextView) rootView.findViewById(R.id.name_text);
			textView.setText(hostSummary.getString("name"));
			
			textView = (TextView) rootView.findViewById(R.id.runtime_connectionstate_text);
			textView.setText(hostSummary.getString("runtime.connectionState"));

		} catch (JSONException e) {			
			Log.e(LOG_TAG, e.getMessage(), e);
		}

		return rootView;
	}
	

}
