package com.moonlight.mobile.login;

public class LoginValidate {
	private static LoginValidate lv = null;
	private boolean isLogin = false;
	private static final Object lockObj = new Object();
	private String userName;
	
	public final static LoginValidate getInstance(){
		synchronized(lockObj){			
			if (lv == null)
				lv = new LoginValidate();
			return lv;
		}
	}
	public boolean checkUser(String url, String username, String password){
		boolean ret = true;
		isLogin = true;
		userName = username;
		return ret;
	}
	public boolean isLogin(){
		return isLogin;
	}
	public String getUserName(){
		return userName;
	}
}
