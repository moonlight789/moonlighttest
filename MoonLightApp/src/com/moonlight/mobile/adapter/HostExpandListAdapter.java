package com.moonlight.mobile.adapter;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.moonlight.mobile.R;

public class HostExpandListAdapter extends BaseExpandableListAdapter{
	private Activity context;
	private List<JSONObject> hosts;
	
	
	public  HostExpandListAdapter(Activity context, List<JSONObject> hosts)	{
		this.context = context;
		this.hosts = hosts;
	}
	
//	public HostExpandListAdapter(Activity context, JSONArray hosts) {
//		super();
//		this.context = context;
//		this.hosts = hosts;
//	}

	@Override
	public int getGroupCount() {
		return hosts.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return hosts.get(groupPosition);
	}

	@Override
	public String getChild(int groupPosition, int childPosition) {
//		return children[groupPosition][childPosition];
		JSONObject obj = (JSONObject) hosts.get(groupPosition);
		
		try {
			return (String) obj.get("name");
		} catch (JSONException e) {
			
			return "Host not available";
		}
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String hostName;
		try {
			hostName = ((JSONObject)this.getGroup(groupPosition)).getString("name");
		} catch (JSONException e) {
			hostName = this.getGroup(groupPosition).toString();
		}
		if(convertView == null){
			LayoutInflater li = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = li.inflate(R.layout.host_layout, null);			
		}
		TextView hostView = (TextView)convertView.findViewById(R.id.host);
		hostView.setTypeface(null,Typeface.BOLD);		
		hostView.setText(hostName);
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		String hostName;
		String osType;
		String memUsage;
		String cpuUsage;
		String vmCount;
		String alarmCount;
		
		try {
			hostName = ((JSONObject)this.getGroup(groupPosition)).getString("name");
			osType = ((JSONObject)this.getGroup(groupPosition)).getString("summary.config.product.osType");
			memUsage = ((JSONObject)this.getGroup(groupPosition)).getString("summary.quickStats.overallMemoryUsage");
			cpuUsage = ((JSONObject)this.getGroup(groupPosition)).getString("summary.quickStats.overallCpuUsage") + context.getResources().getString(R.string.frequency_mhz);
			vmCount = ((JSONObject)this.getGroup(groupPosition)).getString("vm.count");
			alarmCount = "12";
			
		} catch (JSONException e) {
			hostName = "N/A";
			osType= "N/A";
			memUsage = "N/A";
			cpuUsage = "N/A";
			vmCount = "N/A";
			alarmCount = "N/A";
		}
		
		LayoutInflater li = (LayoutInflater)context.getLayoutInflater();
		
		if(convertView == null){
			convertView = li.inflate(R.layout.host_summary, null);
		}
		
		TextView hostItem = (TextView)convertView.findViewById(R.id.hostName);
		hostItem.setText(hostName);
		TextView osTypeItem = (TextView)convertView.findViewById(R.id.osType);
		osTypeItem.setText(osType);
		TextView cpuUsageItem = (TextView)convertView.findViewById(R.id.cpuUsage);
		cpuUsageItem.setText(cpuUsage);
		TextView memUsageItem = (TextView)convertView.findViewById(R.id.memUsage);
		memUsageItem.setText(memUsage);
		TextView vmCountItem = (TextView)convertView.findViewById(R.id.vmCount);
		vmCountItem.setText(vmCount);
		
		TextView alarmCountItem = (TextView)convertView.findViewById(R.id.alarmCount);
		alarmCountItem.setText(alarmCount);

		hostItem.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent swipeTabIntent = new Intent(context.getApplicationContext(),com.moonlight.mobile.HostViewPagerActivity.class);
				context.startActivity(swipeTabIntent);				
			}
			
			
		});
			
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	
}
