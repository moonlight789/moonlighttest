package com.moonlight.mobile.adapter;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.moonlight.mobile.R;

public class AlarmAdapter extends BaseAdapter {
	
	private List<JSONObject> alarmList = null;
	private Context mContext;  
	private Button ackButton;  
	private float x,ux;  
	public AlarmAdapter(Context mContext, List<JSONObject> alarmList) {  
		this.mContext = mContext;  
		this.alarmList = alarmList;  
	}  
	
	
	@Override
	public int getCount() {
		return this.alarmList.size();
	}

	@Override
	public Object getItem(int arg0) {
		return alarmList.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup arg2) {
		ViewHolder viewHolder = null;
		if (view == null) {
			viewHolder = new ViewHolder();
			view = LayoutInflater.from(mContext).inflate(R.layout.vm_alarm_item, null);
			viewHolder.alarmName = (TextView) view.findViewById(R.id.alarm_name);
			viewHolder.alarmStatus = (TextView) view.findViewById(R.id.alarm_status);
			//viewHolder.alarmTriggerTime = (TextView) view.findViewById(R.id.alarm_triggertime);
			viewHolder.ackButton = (Button) view.findViewById(R.id.acknowlege);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();			 
		}
		try {
			viewHolder.alarmName.setText(this.alarmList.get(position).getString("alarm.name"));
			viewHolder.alarmStatus.setText(this.alarmList.get(position).getString("alarm.status"));
			//viewHolder.alarmTriggerTime.setText(this.alarmList.get(position).getString("alarm.triggeredtime"));
		} catch (JSONException e) {
			
		}
		// 为每一个view项设置触控监听
		view.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				final ViewHolder holder = (ViewHolder) v.getTag();
				// 当按下时处理
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					// 设置背景为选中状态
					//v.setBackgroundResource(R.drawablemm_listitem_pressed);
					// 获取按下时的x轴坐标
					x = event.getX();
					// 判断之前是否出现了删除按钮如果存在就隐藏
					if (ackButton != null) {
						ackButton.setVisibility(View.GONE);
					}
				} else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {// 松开处理
					// 设置背景为未选中正常状态
					//v.setBackgroundResource(R.drawable.mm_listitem_simple);
					// 获取松开时的x坐标
					ux = event.getX();
					// 判断当前项中按钮控件不为空时
					// 按下和松开绝对值差当大于20时显示删除按钮，否则不显示
					if (Math.abs(x - ux) > 20) {
						if(holder.ackButton != null){
							holder.ackButton.setVisibility(View.VISIBLE);
							ackButton = holder.ackButton;
						}						
					}else{
						
						Intent eventIntent = new Intent(v.getContext(),com.moonlight.mobile.MLEventsActivity.class);
						ViewHolder vHolder = (ViewHolder)v.getTag();						
						eventIntent.putExtra("alarm_id",vHolder.alarmName.getText().toString());
						v.getContext().startActivity(eventIntent);						
					}
					if (holder.ackButton != null) {
						
					}
				} else if (event.getAction() == MotionEvent.ACTION_MOVE) {// 当滑动时背景为选中状态
					//v.setBackgroundResource(R.drawable.mm_listitem_pressed);
				} else {// 其他模式
					// 设置背景为未选中正常状态
					//v.setBackgroundResource(R.drawable.mm_listitem_simple);
				}
				return true;
			}
		});		
		
		// 为删除按钮添加监听事件，实现点击删除按钮时删除该项
		viewHolder.ackButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (ackButton != null)
					ackButton.setVisibility(View.GONE);
				alarmList.remove(position);
				notifyDataSetChanged();
			}
		});
		return view;
	}
	final static class ViewHolder {  
		//TextView alarmTriggerTime;
		TextView alarmName;
		TextView alarmStatus;
		TextView alarmVM;
		Button ackButton;  
	}  
}
