package com.moonlight.rest.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;

public class HTTPHostThread implements Runnable {
	private String url;
	
	HTTPHostThread(String url){
		this.url = url;
	}
	@Override
	public void run() {
		HTTPRestClient hsc = HTTPRestClient.getInstance();
		String result;
		DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
		HttpGet httpGet = new HttpGet(url);
		//httpClient.getParams().setParameter("http.socket.timeout", 30);
		
		InputStream in = null;
		try {
			HttpResponse httpResp = httpClient.execute(httpGet);
			HttpEntity entity = httpResp.getEntity();
			in = entity.getContent();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(in,"utf-8"));
			StringBuilder sb  = new StringBuilder();
			String line = null;
			while((line= br.readLine())!=null){
				sb.append(line);
			}
			result = sb.toString();
			hsc.setHostArray(new JSONArray(result));
			
		} catch (ClientProtocolException e) {			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			System.err.println("failed to parse the json string");
			e.printStackTrace();
		}finally {
			if(in!=null){
				try {
					in.close();
				} catch (IOException e) {
				}
			}
			
		}
	}

}
