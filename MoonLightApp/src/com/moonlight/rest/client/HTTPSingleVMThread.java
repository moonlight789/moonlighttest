package com.moonlight.rest.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.moonlight.mobile.data.LoginCacheInfo;

public class HTTPSingleVMThread implements Runnable {
	
	private String vmName;
	
	
	
	public HTTPSingleVMThread(String vmName) {
		super();
		this.vmName = vmName;

	}



	@Override
	public void run() {
		
		if(vmName== null || vmName.equals(""))
			return;
					
		HTTPRestClient hsc = HTTPRestClient.getInstance();		
		DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
		String url = LoginCacheInfo.getInstance().getVMInfoRestURLByName(vmName);
		HttpGet httpGet = new HttpGet(url);
		//httpClient.getParams().setParameter("http.socket.timeout", 30);
		
		InputStream in = null;
		try {
			HttpResponse httpResp = httpClient.execute(httpGet);
			HttpEntity entity = httpResp.getEntity();
			in = entity.getContent();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(in,"utf-8"));
			StringBuilder jsonSB  = new StringBuilder();
			String line = null;
			while((line= br.readLine())!=null){
				jsonSB.append(line);
			}
			hsc.setVMObject(new JSONArray(jsonSB.toString()).getJSONObject(0));
			
		} catch (ClientProtocolException e) {			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			System.err.println("failed to parse the json string");
			e.printStackTrace();
		}finally {
			if(in!=null){
				try {
					in.close();
				} catch (IOException e) {
				}
			}
			
		}
	}

}
