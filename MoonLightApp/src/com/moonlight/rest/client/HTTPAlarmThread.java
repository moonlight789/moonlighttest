package com.moonlight.rest.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;

import com.moonlight.mobile.data.LoginCacheInfo;

public class HTTPAlarmThread implements Runnable {
	private String name;
	private int type;
	
	
	public HTTPAlarmThread(String name, int type) {
		super();
		this.name = name;
		this.type = type;
	}
	@Override
	public void run() {
		if(name==null || name.equals(""))
			return;
		LoginCacheInfo licInstance = LoginCacheInfo.getInstance();
		String url = licInstance.getAlarmInfoRestURL(name,type);
		
		HTTPRestClient hsc = HTTPRestClient.getInstance();	
		DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
		HttpGet httpGet = new HttpGet(url);
		//httpClient.getParams().setParameter("http.socket.timeout", 30);
		
		InputStream in = null;
		HttpEntity entity = null;
		try {
			entity = httpClient.execute(httpGet).getEntity();
			if(entity == null){
				hsc.setAlarmArray(null);
				return;
			}
			
			in = entity.getContent();			
			BufferedReader br = new BufferedReader(new InputStreamReader(in,"utf-8"));
			StringBuilder jsonSB  = new StringBuilder();
			String line = null;
			while((line= br.readLine())!=null){
				jsonSB.append(line);
			}
			hsc.setAlarmArray(new JSONArray(jsonSB.toString()));
			
		} catch (ClientProtocolException e) {			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			System.err.println("failed to parse the json string");
			e.printStackTrace();
		}finally {
			if(in!=null){
				try {
					in.close();
				} catch (IOException e) {
				}
			}
			
		}
		
	}

}
