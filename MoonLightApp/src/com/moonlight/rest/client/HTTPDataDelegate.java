package com.moonlight.rest.client;

import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.moonlight.mobile.data.LoginCacheInfo;

public final class HTTPDataDelegate {

	public static String[] fetchHostSummary(String hostName) {
		return new String[] { "Summary data 1", "Summary data 2" };
	}

	public static String[] fetchReservedData() {
		return new String[] { "Reserved data 1", "Reserved data 2" };
	}

	public static String[] fetchAlarmData() {

		return new String[] { "Alarm data 1", "Alarm data 2" };
	}

	public static JSONArray fetchVMData(String hostName) {

		HTTPRestClient hc = HTTPRestClient.getInstance();
		JSONArray ja = hc.getVMsByHost( hostName);
		return ja;
	}
	
	public static JSONObject fetchSingleVMData(String vmName) {

		HTTPRestClient hc = HTTPRestClient.getInstance();

		JSONObject ja = hc.getVMInfoByName(vmName);
		return ja;
	}
	
	public static JSONArray fetchAlarms(String name, int type){
		if (name == null || name.equals(""))
			return null;
		HTTPRestClient hc = HTTPRestClient.getInstance();
		JSONArray ja = hc.getAlarms(name,type);
		return ja;
	}

	private static String[] fetchPerfData() {
		int i = 0;
		try {
			i = Random.class.newInstance().nextInt();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		switch (i % 2) {
		case 0:
			i++;
			return new String[] { "Performance data 1", "Performance data 2" };
		case 1:
			i++;
			return new String[] { "Performance data 1", "Performance data 2",
					"Peformance Data 3" };
		default:
			i++;
			return new String[] { "Performance data 1", "Performance data 2",
					"Peformance Data 3", "Performance data 4" };
		}

	}

}
