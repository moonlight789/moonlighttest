package com.moonlight.rest.client;

import org.json.JSONArray;
import org.json.JSONObject;

public class HTTPRestClient {
	private static HTTPRestClient hc = null;
	
	private JSONArray hostArray = null;
	private JSONArray vmArray = null;
	private JSONObject vmSummary = null;
	private JSONArray alarmArray = null;
	
	public JSONArray getHostArray() {
		return hostArray;
	}

	public void setHostArray(JSONArray hostArray) {
		this.hostArray = hostArray;
	}
	
	

	public JSONArray getVmArray() {
		return vmArray;
	}

	public void setVmArray(JSONArray vmArray) {
		this.vmArray = vmArray;
	}

	public static synchronized HTTPRestClient getInstance(){
		if(hc == null){
			hc = new HTTPRestClient();	
		}
		return hc;
	}
	
	

	public void setVMObject(JSONObject jsonObject) {
		this.vmSummary = jsonObject;
		
	}

	public JSONObject getVmSummary() {
		return vmSummary;
	}
	
	

	public JSONArray getAlarmArray() {
		return alarmArray;
	}

	public void setAlarmArray(JSONArray alarmArray) {
		this.alarmArray = alarmArray;
	}

	public JSONArray getAllHosts(String url){
		new HTTPHostThread(url).run();
		return this.hostArray;		
	}
	public JSONArray getVMsByHost(String hostname){
		new HTTPVMThread(hostname).run();
		return this.getVmArray();
	}

	public JSONObject getVMInfoByName(String vmName) {
		new HTTPSingleVMThread(vmName).run();
		return this.getVmSummary();
	}
	
	public JSONArray getAlarms(String name, int type) {
		new HTTPAlarmThread(name,type).run();
		return this.alarmArray;
	}

	

	
	
	
	
}
